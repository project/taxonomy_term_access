INTRODUCTION
------------

The Taxonomy term access module provides an API for controlling access to
taxonomy terms in a manner similar to the node access system. This module
provides hook_taxonomy_term_grants(), hook_taxonomy_term_access_records(),
hook_taxonomy_term_access_records_alter(), hook_taxonomy_term_grants_alter(),
and hook_taxonomy_term_access().

Beware of "#2073663: Taxonomy load functions filter for access control"
(https://www.drupal.org/project/drupal/issues/2073663). Unlike other entity
types, taxonomy inserts the "taxonomy_term_access" tag in its load function, so
while determining access to a term a module cannot reliably use
taxonomy_term_load() to load terms. This goes for any function that eventually
uses TaxonomyTermController::buildQuery(), such as entity_load(),
entity_load_single(), and entity_metadata_wrapper().

To deal with loading taxonomy terms regardless of access,
TaxonomyTermAccessTaxonomyTermController is provided which extends
TaxonomyTermController. The only change is that the "taxonomy_term_access" tag
is removed from the query. The following functions are provided that use this
class:
- taxonomy_term_access_term_load() to replace taxonomy_term_load()
- taxonomy_term_access_term_load_multiple() to replace
  taxonomy_term_load_multiple()
- taxonomy_term_access_get_tree() to replace taxonomy_get_tree()


REQUIREMENTS
------------

The core Taxonomy module is required.


INSTALLATION
------------

Install as you would normally install a contributed Drupal module. See:
https://drupal.org/documentation/install/modules-themes/modules-7 for
further information.


CONFIGURATION
-------------

There is no configuration.


MAINTAINERS
-----------

Current maintainers:
 * Jacob Embree (jacob.embree) - https://www.drupal.org/u/jacobembree
